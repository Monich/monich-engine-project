# Disable MS warnings for secure functions
add_definitions(-D_CRT_SECURE_NO_WARNINGS)

# Set newest compiler version for Linux
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++11")

# Refuse usage of outdated default libraries
if(MSVC)
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} /NODEFAULTLIB:LIBCMT /NODEFAULTLIB:MSVCRTD")
endif()

# Link to Monich Engine
include(cmake/EngineLinkage.cmake)
