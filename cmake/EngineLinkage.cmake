option(ME_LINK_SOURCE "If true, CMake will look for Monich Engine source files instead of built library" FALSE)

if(ME_LINK_SOURCE)
    message(STATUS "Looking for Monich Engine source directory")

    FIND_PATH(ME_ROOT_DIR
	    NAMES src/ME.cpp src/ME.hpp
        DOC "The Monich Engine root directory"
    )

    if(ME_ROOT_DIR)
        message(STATUS "Monich Engine root directory is ${ME_ROOT_DIR}")
        include("${ME_ROOT_DIR}/monich-engine/CMakeLists.txt")
        include("${ME_ROOT_DIR}/cmake/monich-engineConfig.cmake")
        link_libraries(monich-engine)
        include_directories(${ME_ROOT_DIR})
    else(ME_ROOT_DIR)
        message(FATAL_ERROR "Monich Engine root directory couldn't be located. Please set it in GUI or using console: 'cmake -DME_ROOT_DIR=<directory> .'")
    endif(ME_ROOT_DIR)

else(ME_LINK_SOURCE)
    message(STATUS "Configuring Monich Engine")
    if(ME_DIR)
        set(monich-engine_DIR "${ME_DIR}/lib/cmake/monich-engine")
    endif(ME_DIR)
    find_package(monich-engine CONFIG REQUIRED)
    mark_as_advanced(monich-engine_DIR)
endif(ME_LINK_SOURCE)
