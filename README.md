# Monich Engine Example Project

This is a sample project to be linked to MonichEngine. Use it as the base for all other projects.

# How to link this project to Monich Engine

This project is configured to work with Monich Engine no matter what type of linking you want to use.

## Engine installed in default directory
If Monich Engine is installed in default directory, this project should automatically detect it and link to executable. If engine is not detected look at next section. This option is recommended for standard release.

## Engine installed in other directory
If Monich Engine isn't detected automatically, set ME_DIR variable to the directory, where engine is installed.

## Build from source
This option is recommended for development. Check ME_LINK_SOURCE checkbox or use -DME_LINK_SOURCE=true to enable linking through source code. After that specify the path to root directory in ME_ROOT_DIR variable in GUI or by using -DME_ROOT_DIR on console.

# What else do i need to do

Simply change the name of the project in CMakeLists.txt in the root folder to one of your choice. Feel free to change CMake files. Monich Engine and other external libraries will link to all executables automatically.
