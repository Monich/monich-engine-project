#include <IL/il.h>
#include <glm/glm.hpp>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <monich-engine/ME.hpp>

int main()
{
    // Testing for external libraries

    ilInit();
    glfwInit();
    glewInit();
    return libFunction();
}
